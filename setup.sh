#!/usr/bin/env sh

echo "###"
echo -e "Setting up OPP Environment for Development\nVagrant runs this script in privileged/sudo context"
echo -e "Questions? Contact mmahadevan@okta.com"
echo "###"

#####
# Setup section; Variables are defined here.
#####


# The tomcat deploy path
UPLOADPATH=scim-server-example
MYSQL_DEPLOY=scim-mysql-example

# For Tomcat
MGR_USER=tomcat
MGR_PASSWORD=password

# MySQL Sample Employee File
EMPFILE=employees_db-full-1.0.6.tar.bz2
EMPFILEURL=https://launchpad.net/test-db/employees-db-1/1.0.6/+download/employees_db-full-1.0.6.tar.bz2

# Note that latest OPP Agent and SDK might only be available in Okta Preview Org
OKTAORG=sykle-admin.oktapreview.com

# CHANGE_TO_LATEST: Latest Okta Provisioning Agent as of Oct 15th, 2015
OPAGENTFILE=OktaProvisioningAgent-01.00.11.x86_64.rpm

# CHANGE_TO_LATEST: Latest Okta Provisioning Connector SDK as of Oct 15th, 2015
OPPSDKFILE=Okta-Provisioning-Connector-SDK-01.02.01.zip

# CHANGE_TO_LATEST: Latest Okta Provisioning Connector Tester as of Oct 15th, 2015
OPPTESTERFILE=Okta-Provisioning-Connector-Tester-01.02.01.zip

# CHANGE_TO_LATEST: Latest Okta Provisioning Connector SDK version as of Oct 15th, 2015
# echo `basename /opt/Okta-Provisioning-Connector-SDK/lib/scim-server-sdk-*.jar` | sed  's/.*-\(.*\).jar/\1/'
SCIM_SERVER_SDK_VER=01.02.01

MVNDIR=apache-maven-3.1.1
MVNFILE=apache-maven-3.1.1-bin.tar.gz

# Try wget -q for a quieter operation and a minimal -nv non-verbose option
WGET="wget -q"
# yum will be quiet and will say yes to prompts
YUM="yum -q -y"

TC_CONF_FILE=/usr/share/tomcat6/conf/server.xml
TC_KEYSTOREFILE=/usr/share/tomcat6/conf/scim_tomcat_keystore
TC_KEYSTOREPASS=password
SCIM_CERT=/usr/share/tomcat6/conf/scim_tomcat.cert


#####
# Various Helper Functions
#####

function deploy_to_tomcat {
	curl "http://$MGR_USER:$MGR_PASSWORD@localhost:8080/manager/deploy?path=/$1&war=file:$2&update=true"
}

###
# Simple function to backup a file.
###
function backup {
	cp $1 $1.$(date +%Y%m%d%H%M%S).backup
}

# Needs SDK Installed
function install_mysql {
	$YUM install mysql-server
	service mysqld start
	# service mysqld status
	# mysql_secure_installation

	# Set the mysql root password to "password"
	/usr/bin/mysqladmin -u root password 'password'
	/usr/bin/mysqladmin -u root -h localhost password 'password'
	# If already set, set it again.
	/usr/bin/mysqladmin -u root -p'password' password 'password'
	/usr/bin/mysqladmin -u root -h localhost -p'password' password 'password'

	service mysqld restart

	cd /opt/Okta-Provisioning-Connector-SDK/example-mysql-server/

	if [ ! -f $EMPFILE ]; then
		$WGET $EMPFILEURL
	fi

	tar -xjf ./$EMPFILE
	cd employees_db
	mysql -u root -p'password' -t < employees.sql
	# Test: time mysql -t < test_employees_sha.sql
	cd /opt/Okta-Provisioning-Connector-SDK/example-mysql-server/
	mysql -u root -p'password' -t < employees_schema_alter.sql

	# Do not run mvn silently as we need the name of the war file being generated.
	mvn package | tee mvn-output.txt
	MYSQL_WAR=`awk -F: '/Building war/ {print $2}' ./mvn-output.txt | sed -e 's/^ *//' -e 's/ *$//'`
	rm ./mvn-output.txt
	echo "Generated war file:$MYSQL_WAR"
	deploy_to_tomcat $MYSQL_DEPLOY $MYSQL_WAR
}


function install_setup_tomcat {
	$YUM install tomcat6 tomcat6-webapps tomcat6-admin-webapps
	# Set a default user for Tomcat
	backup /etc/tomcat6/tomcat-users.xml

# okta-opp-agent role is defined here optionally to be used for Basic-AuthN
	cat << EOF > /etc/tomcat6/tomcat-users.xml
<?xml version='1.0' encoding='utf-8'?>
<tomcat-users>
<user name="$MGR_USER" password="$MGR_PASSWORD" roles="admin, manager, okta-opp-agent" />
</tomcat-users>
EOF

	chkconfig tomcat6 on
}

#####
# Needs java, tomcat6, OktaProvisioningAgent to be installed.
# Creates keystore, exports the certificate
#####
function setup_ssl {

# Generate a Java keystore and key pair
if [ ! -f $TC_KEYSTOREFILE ]; then
  keytool -genkey -alias scim_tomcat -keyalg RSA -keystore $TC_KEYSTOREFILE -storepass $TC_KEYSTOREPASS -keypass $TC_KEYSTOREPASS -keysize 2048 -validity 5000 -dname "CN=localhost, OU=ProServ, O=Okta, L=Toronto, S=Ontario, C=CA" -noprompt
fi

if [ ! -f $SCIM_CERT ]; then
# Export the public certificate
  keytool -export -storepass $TC_KEYSTOREPASS -keystore $TC_KEYSTOREFILE -alias scim_tomcat -file $SCIM_CERT -noprompt
# Import the public certificate into the Trusted Key store used by OktaProvisioningAgent
  /opt/OktaProvisioningAgent/jre/bin/keytool -import -file $SCIM_CERT -alias scim_tom -keystore /opt/OktaProvisioningAgent/jre/lib/security/cacerts -storepass changeit -noprompt
fi

}

###
# Needs java, tomcat6, OktaProvisioningAgent to be installed.
# Calls backup routine
# Updates tomcat6 server.xml; Restarts tomcat6
###
function enable_ssl {
	grep scim_tomcat_keystore /usr/share/tomcat6/conf/server.xml > /dev/null

if [ $? -eq 1 ]; then
	backup $TC_CONF_FILE
	read -d '' SSLSTATEMENT << EOF
	Following section added by setup script - @mmaha
-->
    <Connector port="8443" protocol="HTTP/1.1" SSLEnabled="true"
               maxThreads="150" scheme="https" secure="true"
               clientAuth="false" sslProtocol="TLS"
               keystoreFile="$TC_KEYSTOREFILE"
               keystorePass="$TC_KEYSTOREPASS" />
<!--
EOF
        echo "Setting up tomcat6 for SSL"
        # Modifies /usr/share/tomcat6/conf/server.xml
        awk -v sb="$SSLSTATEMENT" \
                        '/<Connector port="8443" protocol="HTTP\/1\.1" SSLEnabled="true"/ { print sb; next } {print}' $TC_CONF_FILE > /usr/share/tomcat6/conf/server.xml.tmp
        mv /usr/share/tomcat6/conf/server.xml.tmp /usr/share/tomcat6/conf/server.xml


	service tomcat6 restart
else
	echo "tomcat6 is configured for SSL"
fi
}

function install_mvn {
	# Install mvn if not installed
	mvn --version | grep "3.1.1"
	if [ $? -eq 1 ]; then
		echo "Installing maven..."
		$WGET http://apache.mirror.vexxhost.com/maven/maven-3/3.1.1/binaries/$MVNFILE
		tar xzf $MVNFILE -C /usr/local
		rm $MVNFILE

		cd /usr/local
		ln -s $MVNDIR maven

		echo -e 'export M2_HOME=/usr/local/maven\nexport PATH=${M2_HOME}/bin:${PATH}' > /etc/profile.d/maven.sh
		echo -e '#!/usr/bin/env bash\n\nexport JAVA_HOME='
		source /etc/profile.d/maven.sh

		echo 'The maven version: ' `mvn -version` ' has been installed.'
		cd ~
	else
		echo "mvn already installed"
	fi
}

function test_inmemory {
	# Testing routine
	cd /opt/Okta-Provisioning-Connector-SDK/tester

	java -jar scim-sdk-tests.jar -url http://localhost:8080/$UPLOADPATH -method downloadUsers
	grep Jensen /opt/Okta-Provisioning-Connector-SDK/tester/downloadUsers*.txt
	if [ $? -eq 0 ]; then
		echo -e "###\nSuccessfully executed downloadUsers and found expected user Jensen\n###"
		rm downloadUsers*.txt
	else
		echo -e "###\nUnexpected result from downloadUsers\n###"
	fi

	java -jar scim-sdk-tests.jar -url http://localhost:8080/$UPLOADPATH -method checkUserExists -arg userIdFieldName=userName -arg userIdFieldValue=okta
	if [ $? -eq 0 ]; then
		echo -e "###\nSuccessfully executed checkUserExists and found expected user okta\n###"
	else
		echo -e "###\nUnexpected result from checkUserExists\n###"
	fi
	cd ~
}

function test_mysql {
	cd /opt/Okta-Provisioning-Connector-SDK/tester
	java -jar scim-sdk-tests.jar -url http://localhost:8080/$MYSQL_DEPLOY -method checkUserExists -arg userIdFieldName=id -arg userIdFieldValue=258642
	if [ $? -eq 0 ]; then
		echo -e "###\nSuccessfully executed checkUserExists and found expected user id 258642\n###"
	else
		echo -e "###\nUnexpected result from checkUserExists\n###"
	fi

	java -jar scim-sdk-tests.jar -url http://localhost:8080/$MYSQL_DEPLOY -method checkUserExists -arg userIdFieldName=userName -arg userIdFieldValue=AamodtAbdelkader@mysql-db.org
	if [ $? -eq 0 ]; then
		echo -e "###\nSuccessfully executed checkUserExists and found expected userName AamodtAbdelkader@mysql-db.org\n###"
	else
		echo -e "###\nUnexpected result from checkUserExists\n###"
	fi
	cd ~
}

function download_agent_sdk {
	# Go back to the home directory
	cd ~

	if [ ! -f $OPAGENTFILE ]; then
			$WGET https://$OKTAORG/static/agents/ProvisioningAgent/$OPAGENTFILE
	fi

	if [ ! -f $OPPSDKFILE ]; then
		$WGET https://$OKTAORG/static/toolkits/$OPPSDKFILE
	fi

	if [ ! -f $OPPTESTERFILE ]; then
		$WGET https://$OKTAORG/static/toolkits/$OPPTESTERFILE
	fi

}

function install_agent {
	cd ~
	# Install only if the file is available
	if [ -f $OPAGENTFILE ]; then
		$YUM localinstall $OPAGENTFILE
	fi
}
function install_sdk {
	cd ~
	if [ -f $OPPSDKFILE ]; then
		# Quietly -q; Overwrites an previous setup -o
		unzip -q -o $OPPSDKFILE -d /opt/Okta-Provisioning-Connector-SDK/

		cd /opt/Okta-Provisioning-Connector-SDK/lib
		/opt/Okta-Provisioning-Connector-SDK/lib/install.sh
		cd ~
		# Check: find /root/.m2/repository/com/okta/scim/sdk/scim-server-sdk/ -name *.jar
	fi

	if [ -f $OPPTESTERFILE ]; then
		# Quietly -q; Overwrites an previous setup -o
		unzip -q -o $OPPTESTERFILE -d /opt/Okta-Provisioning-Connector-Tester/
		# TODO: Need to check if tester unzipped properly
	fi
}

function deploy_inmemory {
	cd /opt/Okta-Provisioning-Connector-SDK/example-server/

	# Do not run mvn silently as we need the name of the war file being generated.
	mvn package | tee mvn-output.txt
	INMEMORY_WAR=`awk -F: '/Building war/ {print $2}' ./mvn-output.txt | sed -e 's/^ *//' -e 's/ *$//'`
	rm ./mvn-output.txt
	echo "Generated war file:$INMEMORY_WAR"

	# The tests after cp sometimes fails as there is no guarantee that the deploy is completed.
	# cp /opt/Okta-Provisioning-Connector-SDK/example-server/target/scim-server-example-01.00.00-SNAPSHOT.war /usr/share/tomcat6/webapps

	deploy_to_tomcat $UPLOADPATH $INMEMORY_WAR

	test_inmemory
	cd ~
}
####################################
### ### Main Routines Start ### ####
####################################

### 1. Download the Agent SDK
download_agent_sdk

### 2. Install the Agent
install_agent

### 3. Some more packages that need to be installed
$YUM install unzip.x86_64
$YUM install java-1.7.0-openjdk.x86_64 java-1.7.0-openjdk-devel.x86_64

install_setup_tomcat
setup_ssl
enable_ssl

install_mvn

/etc/init.d/iptables stop

install_sdk
deploy_inmemory

install_mysql
test_mysql

echo ===
echo Remember to execute sudo /opt/OktaProvisioningAgent/configure_agent.sh
echo ===
