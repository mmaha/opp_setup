Vagrant based Development Environment for On-Premises Provisioning
==================================================================

## Assumptions
- Tested on Centos 6.5 and 6.4 x64.
- OPP SDK and Agent are download from [http://oktapreview.com](http://oktapreview.com)

## Description
This build routine will do the following:

1. Download a minimal Centos based OS VM and configure it for ssh etc. (this is what the Vagrantfile and vagrant software does)
2. Configure with all the packages we need (java, maven, tomcat)
3. Download the OPP Agent and SDK. (Does not configure OPP Agent yet)
4. Build the in-memory and mysql examples from the SDK and deploy it to Tomcat
5. Test that the deployment worked properly. Errors should be displayed... If you hit errors, then it is most likely a bug in my script.

## Instructions
- Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) & [Vagrant](http://www.vagrantup.com/downloads.html)
- `mkdir <whatever>; cd <whatever>`
- Download [Vagrantfile](https://bitbucket.org/mmaha/opp_setup/raw/master/Vagrantfile) & [setup.sh](https://bitbucket.org/mmaha/opp_setup/raw/master/setup.sh)
- Alternatively, if you have `git` installed: `git clone git@bitbucket.org:mmaha/opp_setup.git`
- `vagrant up` Patience here as files are getting downloaded...
- If the port number is right, This link http://localhost:2202/scim-server-example-01.00.00-SNAPSHOT/Users?filter=userName+eq+%22okta%22&startIndex=1&count=100 should return results.
- `vagrant ssh` if you want to change some settings in your guest VM

## Notes
1. mysql is listening on 3306
